<?php
global $current_user, $post, $homey_local;
$current_user = wp_get_current_user();
$dashboard = homey_get_template_link_dash('template/dashboard.php');
$dashboard_profile = homey_get_template_link_dash('template/dashboard-profile.php');
$dashboard_listings = homey_get_template_link_dash('template/dashboard-listings.php');
$dashboard_add_listing = homey_get_template_link_dash('template/dashboard-submission.php');
$dashboard_favorites = homey_get_template_link_dash('template/dashboard-favorites.php');
$dashboard_search = homey_get_template_link_dash('template/dashboard-saved-searches.php');
$dashboard_reservations = homey_get_template_link_dash('template/dashboard-reservations.php');
$dashboard_messages = homey_get_template_link_dash('template/dashboard-messages.php');
$dashboard_invoices = homey_get_template_link_dash('template/dashboard-invoices.php');
$home_link = home_url('/');

?>
<div class="user-dashboard-left white-bg">
    <div class="navi">
        <ul class="board-panel-menu">
            <?php 
            if( !empty($dashboard) ) {
                echo '<li>
                        <a href="'.esc_url($dashboard).'">
                            '.$homey_local['m_dashboard_label'].'
                        </a>
                    </li>';
            }

            if( !empty($dashboard_profile) ) {
                echo '<li class="board-panel-item-active1">
                    <a href="'.esc_url($dashboard_profile).'">
                        '.$homey_local['m_profile_label'].'
                    </a>
                    
                </li>';
                
            }

            if(!homey_is_renter()) {
                if( !empty($dashboard_listings) ) {
                    echo '<li>
                        <a href="'.esc_url($dashboard_listings).'">'.$homey_local['m_listings_label'].'</a>
                    </li>';
                }

                if( !empty($dashboard_add_listing) ) {
                    echo '<li>
                        <a href="'.esc_url($dashboard_add_listing).'">'.$homey_local['m_add_listing_label'].'</a>
                    </li>';
                }
            }

            if( !empty($dashboard_reservations) ) {
                echo '<li>
                    <a href="'.esc_url($dashboard_reservations).'">'.$homey_local['m_reservation_label'].'</a>
                </li>';
            }

            if( !empty($dashboard_messages) ) {
                echo '<li>
                    <a href="'.esc_url($dashboard_messages).'">'.$homey_local['m_messages_label'].'</a>
                </li>';
            }

            if( !empty($dashboard_invoices) ) {
                echo '<li>
                    <a href="'.esc_url($dashboard_invoices).'">'.$homey_local['m_invoices_label'].'</a>
                </li>';
            }

            if( !empty($dashboard_favorites) ) {
                echo '<li>
                    <a href="'.esc_url($dashboard_favorites).'">'.$homey_local['m_favorites_label'].'</a>
                </li>';
            }


            echo '<li>
                <a href="' . wp_logout_url(home_url('/')) . '">'.$homey_local['m_logout_label'].'</a>
            </li>';
            ?>
            
        </ul>
    </div>
</div>