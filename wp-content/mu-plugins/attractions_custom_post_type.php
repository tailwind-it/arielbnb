<?php 
    function arialbnb_post_types() {

        // Attractions Post Type
        register_post_type('attraction', array(
            //'capability_type' => 'attraction', // enables access restriction with user roles
            'map_meta_cap' => true,       // enables access restriction with user roles
            'has_archive' => true,
            'show_in_rest' => true,
            'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
            'rewrite' => array(
                'slug' => 'attractions'
            ),
            'public' => true,
            'labels' => array(
                'name' => 'Attractions',
                'add_new_item' => 'Add New Attraction',
                'edit_item' => 'Edit Attraction',
                'all_items' => 'All Attractions',
                'singular_name' => 'Attraction'
            ),
            'menu_icon' => 'dashicons-location-alt',
            'taxonomies'          => array( 'category', 'post_tag' )
        ));

    }
    add_action('init', 'arialbnb_post_types');
